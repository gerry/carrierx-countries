import ApplicationAdapter from './application';
import $ from 'jquery';

export default ApplicationAdapter.extend({
  buildURL(modelName, id){
    const url = `${this.get('host')}/${this.get('namespace')}/countries/`;
    if(id){
      return `${url}/${id}`;
    }
    return url;
  },

  queryRecord(store, type, query) {
    const url = this.buildURL();
    return $.ajax({
      url: `${url}${query.iso_3166_alpha_3}`,
      type: 'GET',
      contentType: 'application/json',
      dataType: 'json',
      headers: this.get('headers')
    });
  }
});
