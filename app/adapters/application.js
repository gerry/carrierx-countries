import DS from 'ember-data';
import { computed } from '@ember/object';

export default DS.RESTAdapter.extend({
  host: 'https://qa-api.carrierx.com',
  namespace: 'core/v2',
  username: 'gerryg',
  pass: '!Pow677der!33',
  credentials: computed('username', 'pass', function(){
    const base64 = btoa(`${this.get('username')}:${this.get('pass')}`);
    return  `Basic ${base64}`;
  }),

  headers: computed(function(){
    return {
      'Authorization' : this.get('credentials')
    }
  })
});
