import DS from 'ember-data';
const { Model, attr } = DS;

export default Model.extend({
  capital: attr('string'),
  common_name: attr('string'),
  dialing_prefix: attr('string'),
  domain: attr('string'),
  iso_3166_alpha_2: attr('string'),
  iso_3166_alpha_3: attr('string'),
  iso_3166_numeric: attr('number'),
  mcc: attr('number'),
  official_name: attr('string'),
  region: attr('string'),
  subregion: attr('string')
});
