import Controller from '@ember/controller';
import { computed } from '@ember/object';

export default Controller.extend({
  googleAPIKey: '1',
  encodeURIFormat: computed('model.capital', 'model.official_name', function(){
    const searchParams = `?api=${this.get('googleAPIKey')}&query=${this.get('model.official_name')}`;
    return `https://www.google.com/maps/search/${encodeURI(searchParams)}`;
  })
});
