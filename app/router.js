import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('countries', function() {
    this.route('country', { path: '/:iso_3166_alpha_3' });
  });
});

export default Router;
